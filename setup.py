import setuptools

with open("readme.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mp3_bluetooth_player",
    version="0.0.1",
    author="Rafael Finkelstein",
    author_email="finkel.rf@gmail.com",
    description="A RPi zero, OLED mp3 player",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/finkelrf/mp3-bluetooth-player.git",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Linux",
    ],
    install_requires=[
        'adafruit-circuitpython-ssd1306',
        'pillow',
        'mutagen',
        'pygame',
        'tinydb',
        'ipcqueue',
        'systemd'
        #'bluetoothctl-python-wrapper @ git+https://gitlab.com/finkelrf/bluetoothctl-python-wrapper.git'
    ],
    entry_points = {
        'console_scripts': [
            'podplayer=mp3_bluetooth_player.run:main',
            'rootserv=rootserv.run:main'
        ]
}
)