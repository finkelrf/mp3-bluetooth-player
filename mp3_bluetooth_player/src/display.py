from abc import ABC, abstractmethod
import logging
import threading

from PIL import Image, ImageDraw, ImageFont

from mp3_bluetooth_player.config import DEV
from mp3_bluetooth_player.config import DISPLAY_HEIGTH
from mp3_bluetooth_player.config import DISPLAY_WIDTH
from mp3_bluetooth_player.src.position import Position

if not DEV:
    import board
    import busio
    import adafruit_ssd1306
else:
    import pygame
    import io


class DisplayInterface(ABC):
    instance = None
    refresh_timer_started = False

    def __new__(cls):
        '''
        Singleton class for display interface

        Singleton pattern was used to ensure that only one object controls the resource display.
        '''
        if cls.instance is None:
            cls.instance = super(DisplayInterface, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.refresh_loop_start()

    @abstractmethod
    def set_image(self, image, position, name):
        pass

    @abstractmethod
    def clear(self, position):
        pass

    @abstractmethod
    def show(self):
        pass

    @abstractmethod
    def write_text(self, text, position, line, clear_previous, align_right, debug):
        pass

    @abstractmethod
    def get_line_height(self):
        pass

    def refresh_loop_start(self):
        if not self.refresh_timer_started:
            threading.Thread(target=self.show).start()
        self.refresh_timer_started = True


class GraphicalDisplay(DisplayInterface):
    initialized = False
    PROGRESSBAR_MARGIN_Y = 2
    PROGRESSBAR_MARGIN_X = 10

    def __init__(self, width=128, height=32):
        if not self.initialized:
            self._width = width
            self._height = height
            if not DEV:
                self._screen = Oled(self._width, self._height)
            else:
                self._screen = SoftScreen(self._width, self._height)
            self._font = ImageFont.load_default()
            super(GraphicalDisplay, self).__init__()
            self.initialized = True

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    def _clear_line(self, line, position: Position):
        image = Image.new("1", (position.get_dimensions()))
        draw = ImageDraw.Draw(image)
        y_top = self.get_line_height()*line
        rectangle = Position(position.x_left, y_top,
                             position.x_right, position.y_bottom)
        draw.rectangle(rectangle.get_full_tuple(), fill=0, outline=0)
        self._screen.set_image(image, position)

    def clear(self, position: Position):
        image = Image.new("1", (position.get_dimensions()))
        draw = ImageDraw.Draw(image)
        draw.rectangle(position.get_full_tuple(), outline=0, fill=0)
        self._screen.set_image(image, position)

    def show(self):
        self._screen.show()

    def write_text(self, text, position: Position, line=0, clear_previous=True, align_right=False, debug=False):
        image_x = position.get_dimensions()[0]
        image_y = self.get_line_height()
        image = Image.new("1", (image_x, image_y))
        draw = ImageDraw.Draw(image)
        if debug:
            draw.rectangle((0, 0, position.get_dimensions()[
                           0]-1, position.get_dimensions()[1]-1), outline=1)
        x = 0
        if align_right:
            text_length = self._font.getsize(text)[0]
            x = position.get_dimensions()[0] - text_length
        draw.text((x, 0), text.encode('utf-8'),
                  font=self._font, fill=255)
        self._screen.set_image(image, Position(position.x_left, self.get_line_height(
        ) * line + position.y_top, position.x_right, position.y_bottom))

    def get_line_height(self):
        try:
            return self._font.getsize("M")[1]
        except AttributeError:
            return 10

    def get_text_length(self, text):
        try:
            return self._font.getsize(text)[0]
        except AttributeError:
            return 10*len(text)

    def _get_max_progress_bar(self, position):
        return position.x_right - position.x_left - (self.PROGRESSBAR_MARGIN_X*2)

    def draw_progression_bar(self, position: Position, size=None, percent=0):
        image = Image.new("1", position.get_dimensions())
        draw = ImageDraw.Draw(image)

        line_height = self.get_line_height()
        if size is None:
            size = self._get_max_progress_bar(position)

        x1 = position.x_left+self.PROGRESSBAR_MARGIN_X
        y1 = self.PROGRESSBAR_MARGIN_Y

        x2 = x1+size
        y2 = y1+line_height-self.PROGRESSBAR_MARGIN_Y

        x2_progress = x1+(size*float(percent)/100)

        # Draw progression bar ouline
        draw.rectangle((x1, y1, x2, y2), fill=0, outline=1)
        # Draw progress
        draw.rectangle((x1, y1, x2_progress, y2), fill=1, outline=1)
        self._screen.set_image(image, position)

    def draw_border(self, dimension: Position, position: Position):
        image = Image.new("1", dimension.get_dimensions())
        draw = ImageDraw.Draw(image)

        draw.rectangle(dimension.get_full_tuple(), fill=0, outline=1)
        self._screen.set_image(image, position)

    def set_image(self, image, position):
        self._screen.set_image(image, position)


class ScreenInterface(ABC):
    def __init__(self):
        self._image = Image.new("1", (DISPLAY_WIDTH, DISPLAY_HEIGTH))

    @abstractmethod
    def show(self):
        pass

    def set_image(self, image, position):
        self._image.paste(image, position.get_tuple())


class SoftScreen(ScreenInterface):
    def __init__(self, width, height) -> None:
        super().__init__()
        pygame.init()
        self._screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption("Mp3 Player")
        self.lock = threading.Lock()

    def start_mainloop(self):
        self.root.mainloop()

    def show(self):
        self.lock.acquire()
        pygame.display.flip()
        self.lock.release()

    def set_image(self, image, position):
        super().set_image(image, position)
        if not pygame.display.get_init():
            return
        self.lock.acquire()
        self._screen.fill((0, 0, 0))
        image_bytes = io.BytesIO()
        self._image.save(image_bytes, format="PNG")
        image_bytes = image_bytes.getvalue()
        pygame_image = pygame.image.load(io.BytesIO(image_bytes))
        self._screen.blit(pygame_image, (0, 0))
        self.lock.release()


class Oled(ScreenInterface):
    def __init__(self, width=128, height=32):
        super().__init__()
        self._width = width
        self._height = height
        self.lock = threading.Lock()
        i2c = busio.I2C(1, 0)  # i2c0
        # i2c = busio.I2C(board.SCL, board.SDA) #i2c1
        self._screen = adafruit_ssd1306.SSD1306_I2C(
            self._width, self._height, i2c)

    def show(self):
        self.lock.acquire()
        self._screen.show()
        self.lock.release()

    def set_image(self, image, position):
        self.lock.acquire()
        super().set_image(image, position)
        self._screen.image(self._image)
        self.lock.release()
