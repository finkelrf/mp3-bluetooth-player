from mp3_bluetooth_player.config import DEV, HEADER_X, HEADER_Y, DISPLAY_HEIGTH, DISPLAY_WIDTH
from mp3_bluetooth_player.src.controller import ControllerInterface
from mp3_bluetooth_player.src.controller import ControllerTypes
from mp3_bluetooth_player.src.controller import DefaultController
from mp3_bluetooth_player.src.controllers.btctrl import BluetoothSelectionController
from mp3_bluetooth_player.src.controllers.fileselectctrl import FileSelectionController
from mp3_bluetooth_player.src.controllers.playerctrl import PlayerController
from mp3_bluetooth_player.src.position import Position
from mp3_bluetooth_player.src.widgets import BluetoothWidget
from mp3_bluetooth_player.src.widgets import MultilineTextWidget
from mp3_bluetooth_player.src.input import BtMediaKeysInput, HardwareInput, KeyboardInput


class Mp3Screen:
    controller_map = {
        ControllerTypes.DEFAULT: DefaultController,
        ControllerTypes.BLUETOOTH: BluetoothSelectionController,
        ControllerTypes.FILE_SELECTION: FileSelectionController,
        ControllerTypes.PLAYER: PlayerController,
    }

    def __init__(self):
        self.commands = []
        self._bluetooth_widget = BluetoothWidget(position=Position(
            HEADER_X,
            0,
            DISPLAY_WIDTH,
            DISPLAY_HEIGTH))
        # self.battery_widget = BatteryWidget(position=(1, 0))
        self._widget = MultilineTextWidget("", Position(
            0, 0, HEADER_X, DISPLAY_HEIGTH
        ))
        if DEV:
            self.keyboard_input = KeyboardInput(self.commands)
        else:
            self.hw_input = HardwareInput(self.commands)
            self.bt_input = BtMediaKeysInput(self.commands)
        self.controller = DefaultController(self._widget)

    @property
    def controller(self):
        return self._controller

    @controller.setter
    def controller(self, controller: ControllerInterface):
        self._controller = controller
        self._controller.set_widget()

    def handle_input(self, command):
        change_controller, data = self.controller.handle_input(
            command) or (None, None)
        if change_controller:
            self.controller = self.controller_map[change_controller](
                self._widget)
            if data:
                self.controller.set_data(data)
