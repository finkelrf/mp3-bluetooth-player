from glob import glob
import os


class File:
    def __init__(self, name, path=None) -> None:
        self._path = path
        self._name = name

    @property
    def name(self):
        return self._name

    def is_directory(self):
        return "." not in self.name

    def is_empty(self):
        # TODO analize recursivelly if the files in the folder are valid files
        if self._path is None:
            raise Exception("Cannot evaluate if folder is empty. Path is None")
        if self.is_directory():
            return len(os.listdir(os.path.join(self._path, self.name))) == 0
        return False

    def is_mp3(self):
        return self.name[-4:] == ".mp3"

    def __str__(self) -> str:
        return self._name

    def __repr__(self) -> str:
        return str(self)

    def remove(self):
        os.remove(os.path.join(self._path, self._name))

    def has_mp3(self, recursive=True):
        return True if glob(os.path.join(self._path, self._name, '**/*.mp3'), recursive=recursive) else False
