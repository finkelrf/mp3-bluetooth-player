import logging
import os
from abc import ABC, abstractmethod

from mp3_bluetooth_player.config import DEV, ROOT


class Podgrab(ABC):
    def __new__(cls):
        if DEV:
            return super(Podgrab, cls).__new__(PodgrabDocker)
        else:
            return super(Podgrab, cls).__new__(PodgrabService)

    @staticmethod
    @abstractmethod
    def started():
        pass

    @staticmethod
    @abstractmethod
    def start():
        pass

    @staticmethod
    @abstractmethod
    def stop():
        pass


class PodgrabDocker(Podgrab):
    @staticmethod
    def started():
        ret = os.system("docker ps | grep podgrab")
        return True if ret == 0 else False

    @staticmethod
    def start():
        os.system("docker start podgrab")
        logging.debug("Start podgrab")

    @staticmethod
    def stop():
        os.system("docker stop podgrab")
        logging.debug("Stop podgrab")


class PodgrabService(Podgrab):
    @staticmethod
    def started():
        ret = os.system(
            f"systemctl {'' if ROOT else '--user'} is-active podgrab")
        return True if ret == 0 else False

    @staticmethod
    def start():
        os.system(f"systemctl {'' if ROOT else '--user'} start podgrab")
        logging.debug("Start podgrab")

    @staticmethod
    def stop():
        os.system(f"systemctl {'' if ROOT else '--user'} stop podgrab")
        logging.debug("Stop podgrab")
