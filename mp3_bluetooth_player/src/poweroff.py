from ipcqueue import posixmq


def send_poweroff():
    q = posixmq.Queue('/poweroff')
    q.put(True)


def create_poweroff_queue():
    posixmq.Queue('/poweroff')
