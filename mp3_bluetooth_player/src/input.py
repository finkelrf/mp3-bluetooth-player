from enum import Enum, auto
import logging
import signal
import threading
import time
import pygame
from ipcqueue import posixmq

from gpiozero import Button

from mp3_bluetooth_player.config import ENTER_BUTTON_PIN
from mp3_bluetooth_player.config import DOWN_BUTTON_PIN
from mp3_bluetooth_player.config import UP_BUTTON_PIN

LONG_PRESS_INTERVAL_MIN = 300
LONG_PRESS_INTERVAL_MAX = 500
MIN_COMMAND_INTERVAL = 200


class Commands(Enum):
    '''Enumeration of all available commands'''
    LEFT = auto()
    LONG_LEFT = auto()
    CONT_LEFT = auto()
    RIGHT = auto()
    LONG_RIGHT = auto()
    CONT_RIGHT = auto()
    SELECT = auto()
    RETURN = auto()
    PLAY_PAUSE = auto()
    NEXT_MEDIA = auto()
    FORWARD = auto()
    REWIND = auto()
    DO_NOTHING = auto()


class CommandButton:
    '''Class that manages button logic'''

    def __init__(self, gpio, commands, commands_list, continuous_press_enabled=True) -> None:
        self._button = Button(gpio)
        self._short_press_cmd = commands[0]
        self._long_press_cmd = commands[1]
        self._continuous_press_cmd = commands[2]
        self._cmds = commands_list
        self._t0 = 0
        self._t1 = None
        self._last_continous_press_time = None
        self._stop_continuous_press = False
        self._continuous_press_enabled = continuous_press_enabled

        self._button.when_pressed = self._handle_press
        self._button.when_released = self._handle_release

    def continuous_press_thread(self):
        '''Thread that handles continuous button press'''
        while not self._stop_continuous_press:
            now = time.time()
            if (now - self._t0)*1000 > LONG_PRESS_INTERVAL_MAX and \
               (now - self._last_continous_press_time)*1000 > MIN_COMMAND_INTERVAL:
                self._last_continous_press_time = now
                self._cmds.append(self._continuous_press_cmd)
            time.sleep(0.1)

    def _handle_press(self):
        self._t0 = time.time()
        self._last_continous_press_time = self._t0
        self._stop_continuous_press = False
        if self._continuous_press_enabled:
            continuous_press = threading.Thread(
                target=self.continuous_press_thread)
            continuous_press.start()

    def _handle_release(self):
        self._stop_continuous_press = True
        self._t1 = time.time()
        diff = (self._t1 - self._t0)*1000
        if diff < LONG_PRESS_INTERVAL_MIN:
            self._cmds.append(self._short_press_cmd)
            logging.debug(self._short_press_cmd)
        else:
            self._cmds.append(self._long_press_cmd)
            logging.debug(self._long_press_cmd)


class HardwareInput:
    '''Setup hardware input handler'''

    def __init__(self, commands) -> None:
        self.commands = commands
        self._select_button = CommandButton(
            ENTER_BUTTON_PIN, (Commands.SELECT,
                               Commands.RETURN, Commands.DO_NOTHING),
            self.commands, continuous_press_enabled=False)
        self._up_button = CommandButton(
            UP_BUTTON_PIN, (Commands.LEFT, Commands.LONG_LEFT, Commands.CONT_LEFT), self.commands)
        self._down_button = CommandButton(
            DOWN_BUTTON_PIN, (Commands.RIGHT, Commands.LONG_RIGHT, Commands.CONT_RIGHT), self.commands)


class BtMediaKeysInput:
    def __init__(self, commands) -> None:
        super().__init__()
        self.commands = commands
        self.q = posixmq.Queue('/btkeys')
        self.bt_keys_thread = threading.Thread(target=self.handle_bt_keys)
        self.bt_keys_thread.start()

    def handle_bt_keys(self):
        while True:
            cmd = self.q.get()
            logging.debug(f"By media key received {cmd}")
            if cmd == "PLAY_PAUSE":
                self.commands.append(Commands.PLAY_PAUSE)
            elif cmd == "FORWARD":
                self.commands.append(Commands.FORWARD)
            elif cmd == "REWIND":
                self.commands.append(Commands.REWIND)


class KeyPressType(Enum):
    '''Types of key presses'''
    SHORT = 0
    LONG = auto()
    CONTINUOUS = auto()


class Key:
    '''Key class '''

    def __init__(self) -> None:
        self.press_time = None
        self.release_time = None
        self.last_command_time = None

    def __str__(self):
        msg = f"Press: {self.press_time} Release: {self.release_time}"
        if self.release_time and self.press_time:
            msg += f" Diff {(self.release_time - self.press_time)*1000}"
        return msg

    def press(self):
        '''Key pressed'''
        self.press_time = time.time()
        self.last_command_time = self.press_time
        return self

    def release(self):
        '''Key released'''
        self.release_time = time.time()
        return self

    def has_continuous_interval_passed(self):
        '''Check if the continuous command minumum interval has passed'''
        return (time.time() - self.last_command_time)*1000 > MIN_COMMAND_INTERVAL

    def set_command_sent(self):
        '''Set last time a continuous command was send'''
        self.last_command_time = time.time()

    def clear(self):
        '''Clear all key data'''
        self.press_time = None
        self.release_time = None
        return self

    def get_keypress_type(self):
        '''
        Get command based on the time the key was pressed

        0 - LONG_PRESS_MIN                => Short press
        LONG_PRESS_MIN - LONG_PRESS_MAX   => Long press
        > LONG_PRESS_MAX (never released) => Short press (continuous)
        > LONG_PRESS_MAX (released)       => None

        return: Command
            type: Commands or None
        '''
        if not self.press_time:
            return None
        now = time.time()
        if self.press_time and not self.release_time:
            if (now - self.press_time) * 1000 > LONG_PRESS_INTERVAL_MAX:
                if self.has_continuous_interval_passed():
                    self.set_command_sent()
                    return KeyPressType.CONTINUOUS
        else:
            key_pressed_time = (self.release_time - self.press_time)*1000
            if key_pressed_time > LONG_PRESS_INTERVAL_MIN and key_pressed_time < LONG_PRESS_INTERVAL_MAX:
                self.clear()
                return KeyPressType.LONG
            if key_pressed_time < LONG_PRESS_INTERVAL_MIN:
                self.clear()
                return KeyPressType.SHORT
            else:
                self.clear()
        return None


class KeyboardInput:
    key_command_map = {
        pygame.K_UP: (Commands.RIGHT, Commands.LONG_RIGHT, Commands.CONT_RIGHT),
        pygame.K_RIGHT: (Commands.RIGHT, Commands.LONG_RIGHT, Commands.CONT_RIGHT),
        pygame.K_DOWN: (Commands.LEFT, Commands.LONG_LEFT, Commands.CONT_LEFT),
        pygame.K_LEFT: (Commands.LEFT, Commands.LONG_LEFT, Commands.CONT_LEFT),
        pygame.K_RETURN: (Commands.SELECT, Commands.DO_NOTHING, Commands.DO_NOTHING),
        pygame.K_BACKSPACE: (Commands.RETURN, Commands.DO_NOTHING, Commands.DO_NOTHING),
    }

    def __init__(self, commands) -> None:
        super().__init__()
        self.commands = commands
        self._keys = {}
        self._start_read_task()

    def _start_read_task(self):
        self.read_thread = threading.Thread(target=self._read_loop)
        self.read_thread.start()

    def read(self):
        '''Read pygame's button presses'''
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                signal.raise_signal(signal.SIGINT)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    signal.raise_signal(signal.SIGINT)
                else:
                    self._keys[event.key] = Key().press()
            if event.type == pygame.KEYUP:
                if event.key in self._keys:
                    self._keys[event.key].release()

    def get_commands(self):
        '''Get commands based on key presses'''
        key: Key
        for key_name, key in self._keys.items():
            keypress_type = key.get_keypress_type()
            if not keypress_type:
                continue
            if key_name in self.key_command_map:
                command = self.key_command_map[key_name][keypress_type.value]
                self.commands.append(command)

    def _read_loop(self):
        while not pygame.display.get_init():
            logging.debug('Input waiting for display creation')
            time.sleep(1)
        while True:
            self.read()
            self.get_commands()
            time.sleep(0.1)
