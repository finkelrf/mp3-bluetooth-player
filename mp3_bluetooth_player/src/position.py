

class Position:
    def __init__(self, x_left=0, y_top=0, x_right=0, y_bottom=0):
        self._x_left = x_left
        self._y_top = y_top
        self._x_right = x_right
        self._y_bottom = y_bottom

    @property
    def x_left(self):
        return self._x_left

    @x_left.setter
    def x_left(self, pos):
        self._x_left = pos

    @property
    def x_right(self):
        return self._x_right

    @x_right.setter
    def x_right(self, pos):
        self._x_right = pos

    @property
    def y_top(self):
        return self._y_top

    @y_top.setter
    def y_top(self, pos):
        self._y_top = pos

    @property
    def y_bottom(self):
        return self._y_bottom

    @y_bottom.setter
    def y_bottom(self, pos):
        self._y_bottom = pos

    def get_tuple(self):
        return (self.x_left, self.y_top)

    def get_full_tuple(self):
        return (self.x_left, self.y_top, self.x_right, self.y_bottom)

    def copy(self):
        return Position(self.x_left, self.y_top, self.x_right, self.y_bottom)

    def __repr__(self) -> str:
        return str(self)

    def __str__(self) -> str:
        return f"(Position: {self.x_left},{self.y_top},{self.x_right},{self.y_bottom})"

    def get_dimensions(self):
        return (self.x_right-self.x_left, self.y_bottom-self.y_top)
