from mutagen import mp3

from mp3_bluetooth_player.src.utils import Utils
from mp3_bluetooth_player.src.file import File


class Media:
    def __init__(self, filepath) -> None:
        self._length = mp3.MP3(filepath).info.length
        self._name = filepath.split("/")[-1]
        self.folder_path = filepath[:-len(self._name)]
        self._filepath = filepath

    @property
    def folder_path(self):
        return self._folder_path

    @folder_path.setter
    def folder_path(self, value):
        # remove / at the end if necessary
        self._folder_path = value[:-1] if value[-1] == '/' else value

    @property
    def name(self):
        return self._name

    @property
    def length(self):
        ''' Get media length in ms'''
        return self._length

    @property
    def filepath(self):
        return self._filepath

    def get_length_str(self):
        return Utils.s_to_str(self.length)

    def get_file(self):
        return File(self.name, self.folder_path)
