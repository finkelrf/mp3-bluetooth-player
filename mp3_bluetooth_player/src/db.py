import logging
from tinydb import TinyDB, Query

from mp3_bluetooth_player.config import DB_PATH


class Db:
    db = TinyDB(DB_PATH)
    query = Query()

    def __init__(self) -> None:
        if not hasattr(self, "table_name"):
            self.table_name = "default"
        self.table = self.db.table(self.table_name)

    def _insert_or_update(self, data, condition=None):
        found_item = False
        if condition is not None:
            found_item = self.table.get(condition)
        if found_item:
            num_updated = self.table.update(data, condition)
            logging.debug(
                f"Updating: {data} condition {str(condition)}, {num_updated}")
        else:
            logging.debug(f"Inserting: {data}")
            self.table.insert(data)


class PlayerDb(Db):
    table_name = "player"

    def store_media_current_time(self, name, time):
        self._insert_or_update(
            {"name": name, "time": time},
            self.query.name == name)

    def get_media_current_time(self, name):
        item_found = self.table.get(self.query.name == name)
        if item_found and "time" in item_found:
            return item_found['time']
        return None

    def set_media_finished(self, name):
        self._insert_or_update({'name': name, 'finished': True}, self.query.name == name)

