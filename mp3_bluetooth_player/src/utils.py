class Utils:
    @staticmethod
    def s_to_str(s):
        seconds = (s) % 60
        seconds = int(seconds)
        minutes = ((s-seconds)/(60))
        minutes = int(minutes)
        return f"{minutes:03d}:{seconds:02d}"
