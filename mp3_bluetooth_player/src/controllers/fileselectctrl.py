import logging
import os
from mp3_bluetooth_player.config import MEDIA_PATH
from mp3_bluetooth_player.src.controller import ControllerInterface, ControllerTypes
from mp3_bluetooth_player.src.controllers.modalsctrl import RemoveFileModalController
from mp3_bluetooth_player.src.file import File
from mp3_bluetooth_player.src.input import Commands
from mp3_bluetooth_player.src.widgets import MultilineTextWidget


class FileSelectionController(ControllerInterface):
    HEADER = "- Select file"
    DIRECTORY_UP = ".."

    def __init__(self, widget, start_path=None) -> None:
        self._widget: MultilineTextWidget = widget
        self._path = start_path
        if not self._path:
            self._path = MEDIA_PATH
        self._enter_directory(self._path)
        self._modal_controller = None

    def _enter_directory(self, path):
        self._path = path
        self._files = [File(filename, self._path)
                       for filename in os.listdir(self._path)
                       if File(filename, self._path).has_mp3() or File(filename, self._path).is_mp3()]
        logging.debug(self._files)
        self.set_widget()

    def handle_input(self, command):
        if self._modal_controller is not None:
            if self._modal_controller.handle_input(command) is not None:
                self._modal_controller = None
                self._enter_directory(self._path)
        else:
            selected_text = self._widget.get_selected_text()
            if command == Commands.RIGHT or command == Commands.CONT_RIGHT:
                self._widget.selected_down()
            elif command == Commands.LEFT or command == Commands.CONT_LEFT:
                self._widget.selected_up()
            elif command == Commands.SELECT:
                if selected_text == self.HEADER:
                    return ControllerTypes.DEFAULT, None
                elif File(selected_text).is_directory():
                    self._enter_directory(self._path+selected_text)
                elif selected_text == self.DIRECTORY_UP:
                    # Go back one directory
                    self._enter_directory(
                        "/".join(self._path.split("/")[:-1])+"/")
                else:
                    return ControllerTypes.PLAYER, os.path.join(self._path, selected_text)
            elif command == Commands.RETURN:
                if File(selected_text).is_mp3():
                    self._modal_controller = RemoveFileModalController(
                        self._widget._position, File(selected_text, self._path))

    def set_widget(self):
        self._widget.clear()
        if self._path == MEDIA_PATH:
            self._widget.set_text(self.HEADER, 0)
        else:
            self._widget.set_text(self.DIRECTORY_UP, 0)
        self._widget.set_text_lines([file.name for file in self._files], 1)
        self._widget.show()
