from itertools import cycle
import logging
import threading
import time
import re

from bluetoothctlwrapper import Bt

from mp3_bluetooth_player.src.controller import ControllerInterface
from mp3_bluetooth_player.src.controller import ControllerTypes
from mp3_bluetooth_player.src.input import Commands
from mp3_bluetooth_player.src.widgets import MultilineTextWidget


class BluetoothSelectionController(ControllerInterface):
    HEADER = "- Bluetooth"
    SCANNING = "Scanning"

    def __init__(self, widget):
        self._widget: MultilineTextWidget = widget
        self._is_scanning_animation_on = False
        self._bt = Bt()
        self._start_bluetooth_scanner()
        self._scan_aborted = False
        self._devices = {}

    def _filter_devices(self):
        ''' Remove unnamed devices '''
        discarded_devices = []
        for device in self._devices.keys():
            match = re.match(r"([0-9a-fA-F]-?){12}", device)
            if match:
                logging.debug(f"Removing {device}")
                discarded_devices.append(device)
        [self._devices.pop(device) for device in discarded_devices]

    def _scan_bluetooth(self):
        self._bt.scan_on(2)
        self._devices = self._bt.get_devices()
        if not self._scan_aborted:
            self._filter_devices()
            self._is_scanning_animation_on = False
            self._widget.set_text_lines(self._devices.keys(), 1)
            self._widget.show()

    def _start_bluetooth_scanner(self):
        threading.Thread(target=self._scan_bluetooth).start()

    def _connect_to_bluetooth_device(self, device_name):
        if device_name in self._devices:
            logging.debug("Connecting to device %s", device_name)
            try:
                return self._bt.connect(self._devices[device_name], timeout=10)
            except TimeoutError:
                return False
        else:
            logging.error(f"Couldn't connect to {device_name}")

    def set_widget(self):
        self._widget.clear()
        self._widget.set_text(self.HEADER, 0)
        self._widget.set_text("Scanning ", 1)
        self.start_scanning_animation()
        self._widget.show()

    def do_scanning_animation(self):
        animation_dots = "."
        while self._is_scanning_animation_on:
            self._widget.set_text(f"{self.SCANNING} {animation_dots}", 1)
            self._widget.show()
            if animation_dots != "....":
                animation_dots += "."
            else:
                animation_dots = "."
            time.sleep(0.5)

    def do_pairing_animation(self):
        sequence = cycle(['\\', '|', '/', '-'])
        text = f'({next(sequence)}){self._widget.get_selected_text()}'
        line = self._widget.get_selected_line_number()
        while self._is_pairing_animation_on:
            self._widget.set_text(text, line)
            self._widget.show()
            text = f'({next(sequence)}){text[3:]}'
            time.sleep(0.5)
        self._widget.set_text(text[3:], line)
        self._widget.show()

    def start_pairing_animation(self):
        self._is_pairing_animation_on = True
        threading.Thread(target=self.do_pairing_animation).start()

    def start_scanning_animation(self):
        self._is_scanning_animation_on = True
        threading.Thread(target=self.do_scanning_animation).start()

    def handle_input(self, command):
        if command == Commands.RIGHT:
            self._widget.selected_down()
            logging.debug(self._widget.selected)
        elif command == Commands.LEFT:
            self._widget.selected_up()
        elif command == Commands.SELECT:
            if self._widget.get_selected_text() == self.HEADER:
                self._is_scanning_animation_on = False
                self._scan_aborted = True
                return ControllerTypes.DEFAULT, None
            elif self._widget.get_selected_text() in self._devices:
                selected_device = self._widget.get_selected_text()
                self.start_pairing_animation()
                connected = self._connect_to_bluetooth_device(selected_device)
                if connected:
                    self._is_pairing_animation_on = False
                    logging.debug(f"Bluetooth device connected")
                    return ControllerTypes.DEFAULT, None
                else:
                    self._is_pairing_animation_on = False
                    logging.debug(f"Bluetooth NOT connected")
