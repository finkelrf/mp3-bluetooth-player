from mp3_bluetooth_player.src.controller import ControllerInterface
from mp3_bluetooth_player.src.input import Commands
from mp3_bluetooth_player.src.widgets import ModalWidget


class ModalController(ControllerInterface):
    def __init__(self, position, text, options) -> None:
        super().__init__()
        self._widget = ModalWidget(position, options, text)

    def handle_input(self, command):
        pass

    def set_widget(self):
        pass


class RemoveFileModalController(ModalController):
    def __init__(self, position, file, text="Remove file?", options=("yes", "no")) -> None:
        super().__init__(position, text, options)
        self._file = file

    def handle_input(self, command: Commands):
        '''
        Handle input when RemoveFileModalController is selected

        command: Command input

        return: True if file file was deleted
                False if file was not deleted
                None if no decision was made

        note: After a True or False is returned modal should be destroyed
        '''
        selected = self._widget.get_selected_option()
        if command == Commands.SELECT:
            if selected == 'yes':
                self._file.remove()
                return True
            else:
                return False
        elif command == Commands.LEFT or command == Commands.RIGHT:
            self._widget.toggle_selected()
