import logging
import threading
from mp3_bluetooth_player.src.controller import ControllerInterface, ControllerTypes
from mp3_bluetooth_player.src.controllers.modalsctrl import RemoveFileModalController
from mp3_bluetooth_player.src.input import Commands
from mp3_bluetooth_player.src.media import Media
from mp3_bluetooth_player.src.player import Player
from mp3_bluetooth_player.src.widgets import PlayerWidget


class PlayerController(ControllerInterface):
    def __init__(self, widget):
        widget.clear()
        self._remove_media_modal_ctrl = None
        self._media = None
        self._player = None
        self._widget = None

    def set_data(self, media_path):
        self._media = Media(media_path)
        self._player = Player(self._media)
        self._player.set_media_finished_callback(self.open_remove_media_modal)
        self._widget = PlayerWidget(self._player)

    def open_remove_media_modal(self):
        self._widget.stop_updating_thread()
        self._remove_media_modal_ctrl = RemoveFileModalController(
            self._widget._position, self._player.media.get_file())

    def set_widget(self):
        pass

    def handle_input(self, command):
        if self._remove_media_modal_ctrl:
            if self._remove_media_modal_ctrl.handle_input(command) is not None:
                self._remove_media_modal_ctrl = None
                return ControllerTypes.DEFAULT, None
        else:
            if command == Commands.RIGHT:
                self._player.volume_up()
            elif command == Commands.LEFT:
                self._player.volume_down()
            elif command == Commands.LONG_LEFT or \
                    command == Commands.REWIND or \
                    command == Commands.CONT_LEFT:
                self._player.rewind()
            elif command == Commands.LONG_RIGHT or \
                    command == Commands.FORWARD or \
                    command == Commands.CONT_RIGHT:
                self._player.forward()
            elif command == Commands.SELECT or \
                    command == Commands.PLAY_PAUSE:
                self._player.toggle()
            elif command == Commands.RETURN:
                self._player.stop()
                self._widget.stop()
                return ControllerTypes.DEFAULT, None
