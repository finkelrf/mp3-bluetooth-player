from ast import List, Tuple
import logging
import os
import threading
import time
from abc import ABC, abstractmethod

from PIL import Image, ImageOps

import mp3_bluetooth_player
from mp3_bluetooth_player.src.bluetooth import Bluetooth
from mp3_bluetooth_player.src.display import DisplayInterface
from mp3_bluetooth_player.src.display import GraphicalDisplay
from mp3_bluetooth_player.src.player import Player
from mp3_bluetooth_player.src.position import Position
from mp3_bluetooth_player.config import HEADER_X
from mp3_bluetooth_player.config import DISPLAY_HEIGTH


class Widget(ABC):
    def __init__(self, position):
        self._position: Position = position
        self._display: GraphicalDisplay = GraphicalDisplay()

    def show(self):
        self._display.show()

    def clear(self):
        self._display.clear(self._position)

    def update(self):
        ''' This is not an @abstractmethod, because we want to be able to instantiate Widget()'''
        raise Exception("Update not implemented")


class ModalWidget(Widget):
    MARGIN_X = 10
    MARGIN_Y = 5

    def __init__(self, position: Position, options, msg: str):
        super().__init__(position)
        self._options = options
        self._msg = msg
        self._dimension = Position(
            0, 0, position.x_right-(self.MARGIN_X*2), position.y_bottom-(self.MARGIN_Y*2))
        self._selected = 1
        self.update()

    @property
    def selected(self):
        return self._selected

    def toggle_selected(self):
        self._selected = 0 if self.selected == 1 else 1
        self.update()

    def get_selected_option(self):
        return self._options[self.selected]

    def update(self):
        pos = self._position.copy()
        pos.x_left += self.MARGIN_X
        pos.y_top += self.MARGIN_Y
        pos.x_right -= self.MARGIN_X
        pos.y_bottom -= self.MARGIN_Y
        self._display.draw_border(self._dimension, pos)

        text_marging_x = 10
        text_marging_y = 1
        # write modal msg
        self._display.write_text(self._msg, Position(
            pos.x_left+text_marging_x, pos.y_top+text_marging_y, pos.x_right-text_marging_x, pos.y_bottom-text_marging_y))

        # write options
        self._display.write_text(self._options[0], Position(
            pos.x_left+text_marging_x,
            pos.y_top+text_marging_y,
            pos.x_left+text_marging_x +
            self._display.get_text_length(self._options[0]),
            pos.y_bottom-text_marging_y), 1, debug=self.selected == 0)
        self._display.write_text(self._options[1], Position(
            pos.x_right-text_marging_x -
            self._display.get_text_length(self._options[1]),
            pos.y_top+text_marging_y,
            pos.x_right-text_marging_x,
            pos.y_bottom-text_marging_y), 1, align_right=True, debug=self.selected == 1)
        self.show()


class ProgressBarWidget(Widget):
    def __init__(self, position, percentage=0):
        super().__init__(position)
        self._percentage = percentage

    @property
    def percentage(self):
        return self._percentage

    @percentage.setter
    def percentage(self, percentage):
        self._percentage = percentage
        self.update()

    def update(self):
        self._display.draw_progression_bar(position=self._position,
                                           percent=self.percentage)
        self.show()


class SimpleTextWidget(Widget):
    def __init__(self, text, position, align_right=False, debug=False):
        super().__init__(position)
        self._debug = debug
        self._align_right = align_right
        self._text = text
        self.update()

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text
        self.update()

    def update(self):
        self._display.write_text(
            self._text, self._position, align_right=self._align_right, debug=self._debug)
        self.show()


class PlayerWidget(Widget):
    def __init__(self, player, position=None):
        if position is None:
            position = Position(0, 0, HEADER_X, DISPLAY_HEIGTH)
        super().__init__(position)
        self._player: Player = player
        self._player.play()
        self._thread_stop = False

        # internal widgets
        line_heigth = 10
        self._progressbat_widget = ProgressBarWidget(Position(
            self._position.x_left,
            self._position.y_top+line_heigth-1,
            self._position.x_right,
            self._position.y_top+line_heigth+line_heigth+1
        ))
        self._title_widget = SimpleTextWidget(self._player.media.name, Position(
            self._position.x_left,
            self._position.y_top,
            self._position.x_right,
            self._position.y_top+line_heigth
        ))
        self._time_playing_widget = SimpleTextWidget(self._player.get_position_str(), Position(
            self._position.x_left,
            self._position.y_top+(line_heigth*2)+2,
            int(self._position.x_right/2),
            self._position.y_bottom
        ))
        self._media_length_widget = SimpleTextWidget(self._player.media.get_length_str(), Position(
            int(self._position.x_right/2),
            self._position.y_top+(line_heigth*2),
            self._position.x_right,
            self._position.y_bottom
        ), align_right=True)

        threading.Thread(target=self.update_thread).start()

    def _write_text(self, text, position=None, line=0, clear_previous=False):
        if not position:
            position = self._position
        self._display.write_text(text, position, line, clear_previous)

    def stop_updating_thread(self):
        self._thread_stop = True

    def update_thread(self):
        while not self._thread_stop:
            self.update()
            time.sleep(0.5)

    def update(self):
        self._progressbat_widget.percentage = self._player.get_percent()
        self._time_playing_widget.text = self._player.get_position_str()

    def stop(self):
        self._display.clear(self._position)
        self._thread_stop = True


class MultilineTextWidget(Widget):
    def __init__(self, text, position, max_lines=3, selected=0):
        super().__init__(position=position)
        self._max_lines = max_lines
        self._selected = selected
        self._lines_text = []
        self._first_showing_item = 0
        if text:
            self.set_text(text)

    @property
    def selected(self):
        return self._selected

    @selected.setter
    def selected(self, line):
        if line < 0:
            line = 0
        elif line >= len(self._lines_text):
            line = len(self._lines_text) - 1
        self._selected = line
        self.update()
        self.show()

    def get_selected_text(self):
        return self._lines_text[self.selected]

    def get_selected_line_number(self):
        return self.selected

    def clear(self):
        self._selected = 0
        self._lines_text = []
        self._display.clear(self._position)

    def set_text_lines(self, text_lines, first_line):
        for i, text in enumerate(text_lines):
            try:
                self._lines_text[first_line+i] = text
            except IndexError:
                self._lines_text.append(text)
        self.update()

    def set_text(self, text, line=0):
        try:
            self._lines_text[line] = text
        except IndexError:
            self._lines_text.append(text)
        self.update()

    def update(self):
        if self._selected < self._first_showing_item:
            self._first_showing_item = self._selected
        elif self._selected > self._first_showing_item+self._max_lines-1:
            self._first_showing_item = self._selected - self._max_lines + 1

        for i in range(self._first_showing_item, self._first_showing_item+self._max_lines):
            if i < len(self._lines_text):
                text = self._lines_text[i]
                if i == self.selected:
                    text = ">" + text
                self._display.write_text(
                    text, position=self._position, line=i-self._first_showing_item)

    def selected_up(self):
        self.selected = self.selected-1

    def selected_down(self):
        self.selected = self.selected+1

    def unselect(self):
        self._selected = -1
        self.update()
        self.show()


class BatteryWidget(Widget):
    def update(self):
        pass


class BluetoothWidget(Widget):
    def __init__(self, position):
        super().__init__(position)
        self.is_on = False
        self.connected_device = None
        self.update()
        threading.Thread(target=self._update_task).start()

    def _update_task(self):
        while True:
            self.update()
            time.sleep(1)

    def __str__(self) -> str:
        return f"Bluetooth: is on: {self.is_on} | device: {self.connected_device}"

    def update(self):
        icons_path = os.path.join(os.path.dirname(
            mp3_bluetooth_player.__file__), "icons")
        if Bluetooth.is_connected():
            image_path = os.path.join(icons_path, "bluetooth.bmp")
        else:
            image_path = os.path.join(icons_path, "cancel.bmp")
        image = Image.open(image_path)
        image = image.convert('L')
        image = ImageOps.invert(image)
        image = image.convert("1")
        self._display.set_image(image, self._position)
