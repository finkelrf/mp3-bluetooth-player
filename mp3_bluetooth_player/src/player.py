import logging
import time
import threading

from pygame import mixer

from mp3_bluetooth_player.src.db import PlayerDb
from mp3_bluetooth_player.src.utils import Utils
from mp3_bluetooth_player.src.media import Media


class Player:
    INITIAL_VOLUME = 0.5
    VOLUME_STEP = 0.1
    POSITION_STEP = 30.0

    def __init__(self, media) -> None:
        self._media: Media = media
        self._time_playing = 0.0  # position in seconds
        self._position_offset = 0.0  # in seconds
        self._media_finished_callback = lambda: None
        stored_time = PlayerDb().get_media_current_time(self._media.name)
        if stored_time:
            self._position_offset = float(stored_time)
        self._playing = False

        mixer.init()
        mixer.music.load(self._media.filepath)
        mixer.music.set_volume(self.INITIAL_VOLUME)

    @property
    def media(self):
        return self._media

    @property
    def playing(self):
        return self._playing

    @property
    def position(self):
        ''' Get current position in s'''
        position = self._time_playing + self._position_offset
        if position < 0:
            self._position_offset = self._time_playing*-1
            position = 0
        return position

    def set_media_finished_callback(self, callback):
        self._media_finished_callback = callback

    def get_position_str(self):
        return Utils.s_to_str(self.position)

    def get_percent(self):
        return 100*self.position/self._media.length

    def play_thread(self):
        while self._playing:
            mixer_pos = mixer.music.get_pos()/1000
            logging.debug(
                f'Time playing: {self._time_playing} - Offset {self._position_offset} - postion {self.position}')
            if mixer_pos < 0:
                # Media is finished
                PlayerDb().set_media_finished(self._media.name)
                self._playing = False
                mixer.music.stop()
                self._media_finished_callback()
            else:
                self._time_playing = mixer_pos
                time.sleep(0.5)

    def play(self):
        self._playing = True
        mixer.music.play(start=float(self.position))
        threading.Thread(target=self.play_thread).start()

    def pause(self):
        self._playing = False
        self._position_offset += self._time_playing
        self._time_playing = 0
        mixer.music.pause()

    def stop(self):
        self._playing = False
        self._position_offset += self._time_playing
        self._time_playing = 0
        PlayerDb().store_media_current_time(self.media.name, self.position)
        mixer.music.stop()

    def volume_up(self):
        vol = mixer.music.get_volume()
        mixer.music.set_volume(vol+self.VOLUME_STEP)
        logging.debug(f"Volume {vol+self.VOLUME_STEP}")

    def volume_down(self):
        vol = mixer.music.get_volume()-self.VOLUME_STEP
        if vol < 0:
            vol = 0
        mixer.music.set_volume(vol)
        logging.debug(f"Volume {vol}")

    def forward(self):
        position = self.position+self.POSITION_STEP
        if position < self.media.length:
            mixer.music.set_pos(position)
            self._position_offset += self.POSITION_STEP

    def rewind(self):
        position = self.position-self.POSITION_STEP
        mixer.music.set_pos(position)
        self._position_offset -= self.POSITION_STEP

    def toggle(self):
        if self.playing:
            self.pause()
        else:
            self.play()
