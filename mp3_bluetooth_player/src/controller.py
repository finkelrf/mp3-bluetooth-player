from abc import ABC, abstractmethod
from enum import Enum
import logging
import signal

from mp3_bluetooth_player.config import DEV, DISPLAY_HEIGTH, DISPLAY_WIDTH
from mp3_bluetooth_player.src.input import Commands
from mp3_bluetooth_player.src.podgrab import Podgrab
from mp3_bluetooth_player.src.position import Position
from mp3_bluetooth_player.src.poweroff import send_poweroff
from mp3_bluetooth_player.src.widgets import Widget
from mp3_bluetooth_player.src.widgets import MultilineTextWidget


class ControllerTypes(Enum):
    DEFAULT = 0
    BLUETOOTH = 1
    FILE_SELECTION = 3
    PLAYER = 4


class ControllerInterface(ABC):
    @abstractmethod
    def handle_input(self, command):
        pass

    @abstractmethod
    def set_widget(self):
        pass

    def set_data(self):
        pass


class DefaultController(ControllerInterface):
    BLUETOOTH = "Bluetooth"
    PLAY_PODCAST = "Play Podcast"
    PODGRAB_START = "Podgrab Start"
    PODGRAB_STOP = "Podgrab Stop"
    OTHERS = "Others"
    POWER_OFF = "Power Off"

    def __init__(self, widget):
        self._widget: MultilineTextWidget = widget

    def set_widget(self):
        self._widget.clear()
        self._widget.set_text(self.BLUETOOTH, 0)
        self._widget.set_text(self.PLAY_PODCAST, 1)
        if Podgrab().started():
            self._widget.set_text(self.PODGRAB_STOP, 2)
        else:
            self._widget.set_text(self.PODGRAB_START, 2)
        self._widget.set_text(self.POWER_OFF, 3)
        self._widget.set_text(self.OTHERS, 4)
        self._widget.show()

    def handle_input(self, command):
        if command == Commands.RIGHT or command == Commands.CONT_RIGHT:
            self._widget.selected_down()
            logging.debug(self._widget.selected)
        elif command == Commands.LEFT or command == Commands.CONT_LEFT:
            self._widget.selected_up()
        elif command == Commands.SELECT:
            if self._widget.get_selected_text() == self.BLUETOOTH:
                return ControllerTypes.BLUETOOTH, None
            elif self._widget.get_selected_text() == self.PLAY_PODCAST:
                return ControllerTypes.FILE_SELECTION, None
            elif self._widget.get_selected_text() == self.PODGRAB_START:
                Podgrab().start()
                self.set_widget()
            elif self._widget.get_selected_text() == self.PODGRAB_STOP:
                Podgrab().stop()
                self.set_widget()
            elif self._widget.get_selected_text() == self.POWER_OFF:
                if DEV:
                    signal.raise_signal(signal.SIGINT)
                else:
                    widget = Widget(
                        Position(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGTH))
                    widget.clear()
                    widget.show()
                    send_poweroff()
