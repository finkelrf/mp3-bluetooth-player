import re
import subprocess


class Bluetooth:
    @staticmethod
    def is_connected():
        output = subprocess.check_output(["hcitool", "con"])
        group = re.search(
            r"(\w\w\:\w\w\:\w\w\:\w\w\:\w\w\:\w\w)", output.decode())
        return True if group is not None else False
