from unittest.mock import patch
from mp3_bluetooth_player.src.media import Media


def test_one():
    assert True == True


@patch('mutagen.mp3.MP3', autospec=True)
def test_media_constructor(mock_mutagem_mp3):
    media = Media('/my/test/path/media.mp3')

    assert media.folder_path == '/my/test/path'
    assert media.name == 'media.mp3'
    assert media.filepath == '/my/test/path/media.mp3'
