import time
import pytest
from unittest.mock import patch

from mp3_bluetooth_player.src.input import Key, KeyPressType


# class TestKeyboardInput:
#     def test_


class TestKeyClass:
    @pytest.mark.parametrize("press_duration,release,expected_keypress_type", [
        (0.1,  True,  KeyPressType.SHORT),
        (0.3,  True,  KeyPressType.LONG),
        (0.51, True,  None),
        (0.51, False, KeyPressType.CONTINUOUS),
    ])
    def test_key_press(self, press_duration, release, expected_keypress_type):
        '''
        Test key press types
        '''
        # Arrange
        key = Key()

        # Act
        key.press()
        time.sleep(press_duration)
        if release:
            key.release()

        # Assert
        assert key.get_keypress_type() == expected_keypress_type

    def test_continuos_press(self):
        '''
        Test continuous press
        '''
        # Arrange
        key = Key()

        # Act
        key.press()
        time.sleep(0.501)

        # Assert
        assert key.get_keypress_type() == KeyPressType.CONTINUOUS

        # Act
        key.release()

        # Assert
        assert key.get_keypress_type() is None
        assert key.press_time is None
        assert key.release_time is None
