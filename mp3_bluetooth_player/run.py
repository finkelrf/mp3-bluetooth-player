import logging
import signal
import time

from systemd import journal

from .config import DEV
from .src.screen import Mp3Screen
from .src.poweroff import create_poweroff_queue


def handler(signum, frame):
    '''
    Gracefully close podplayer
    '''
    if DEV:
        import pygame
        pygame.quit()
    exit()


def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(name)s %(message)s')
    logger = logging.getLogger(__name__)
    logger.addHandler(journal.JournaldLogHandler())
    logging.info("Program starting")
    signal.signal(signal.SIGINT, handler)
    create_poweroff_queue()

    try:
        mp3 = Mp3Screen()
        while True:
            time.sleep(0.05)
            if mp3.commands:
                command = mp3.commands.pop(0)
                mp3.handle_input(command)

    except Exception as e:
        logger.error(e, exc_info=True)


if __name__ == "__main__":
    main()
