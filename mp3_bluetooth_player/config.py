import platform
import os

ROOT = True if os.environ['USER'] == "root" else False

DEV = True
if platform.machine() == 'armv6l':
    DEV = False

if DEV:
    MEDIA_PATH = "/home/rafael/Documents/podgrab/data/"
else:
    MEDIA_PATH = "/usr/local/bin/podgrab/assets/"

ENTER_BUTTON_PIN = 17
UP_BUTTON_PIN = 16
DOWN_BUTTON_PIN = 3

DISPLAY_WIDTH = 128
DISPLAY_HEIGTH = 32
HEADER_X = DISPLAY_WIDTH - 16
HEADER_Y = 0

if DEV:
    DB_PATH = 'podplayerdb.json'
else: 
    DB_PATH = '/var/lib/podplayer/podplayerdb.json'

#User config
#TODO Create a screen to edit user config
AUTO_DELETE_PODCAST_MEDIA_WHEN_FINISHED = True
