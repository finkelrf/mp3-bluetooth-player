# Requirements

## RPI
sudo apt install libsdl2-mixer-2.0-0
sudo apt-get install pulseaudio-module-bluetooth

## Development
mount sshfs
- sudo sshfs -o allow_other rafael@note-rafael.local:Documents/mp3-bluetooth-player mp3-player-mnt/


# Installation instructions

## System configuration before installing Podplayer
Some changes are necessary in the raspberry pi OS before installing Podplayer
### Install pulseaudio and libsdl2
```
sudo apt install libsdl2-mixer-2.0-0
sudo apt-get install pulseaudio-module-bluetooth
```

### Install Grabpod
The podcast agregation is done via grabpob
- [Go installation](https://www.jeremymorgan.com/tutorials/raspberry-pi/install-go-raspberry-pi/)
- [Grabpod installation](https://github.com/akhilrex/podgrab/blob/master/docs/ubuntu-install.md)
## Install PodPlayer
```
#clone into rpi
git clone git@gitlab.com:finkelrf/mp3-bluetooth-player.git podplayer
cd podplayer
pip install . --system
podplayer
```

## Auto start Podplayer
```
#Copy the podplayer.service systemd
cp podplayer/podplayer.service  ~/.config/systemd/user/

systemctl --user enable podplayer.service
systemctl --user start podplayer.service

#Copy the podplayer-rootserv.service systemd
sudo cp podplayer/podplayer-rootserv.service /etc/systemd/system/

sudo systemctl enable podplayer-rootserv.service
sudo systemctl start podplayer-rootserv.service
```

Enable rpi auto login (without this the podplayer user service does not start at boot) following the instructions below.
Run: sudo raspi-config
Choose option: 1 System Options
Choose option: S5 Boot / Auto Login
Choose option: B2 Console Autologin
Select Finish, and reboot the Raspberry Pi.

# Features
- [x] Connect to Bluetooth
- [x] Select and Play Mp3
- [x] Delete media
- [ ] Connect/disconect to wifi
- [x] Start/Stop grabpod
- [x] Icon for bt
- [ ] Icon for wifi
- [ ] Icon for battery/power
- [x] Continue podcast where stopped
- [ ] Podgrab on hotspot
- [ ] Add Lidarr
- [ ] Sync media with a network folder
- [x] Gracefull power off
- [x] Power on
- [ ] Add supoport to ssd1306 128x64 display
- [ ] Informations menu (IP, podgrab address)
- [x] Read bluetooth inputs (play/pause, volume, forward/rewind) (Only with sudo)
- [x] Make it installable with pip
- [ ] Add battery voltage reader
- [ ] Add automatic turn off on low battery
- [ ] 100% disconnect from battery (remove residual consumption ~2 mA)
- [ ] Play/Download audiobooks
