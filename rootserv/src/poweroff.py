import os
from ipcqueue import posixmq


def wait_for_poweroff():
    q = posixmq.Queue('/poweroff')
    msg = False
    while msg is not True:
        msg = q.get()
    os.system("poweroff")
