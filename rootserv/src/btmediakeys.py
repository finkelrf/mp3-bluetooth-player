import keyboard
import logging
from ipcqueue import posixmq


class BtKeys():
    def __init__(self) -> None:
        self.input_triggered = False
        self.q = posixmq.Queue('/btkeys')
        keyboard.hook(self.handle_bt_keys)

    def handle_bt_keys(self, event: keyboard.KeyboardEvent):
        if event.event_type == "up":
            logging.debug(f"Bt Key pressed: {event.scan_code}")
            if event.scan_code == 200 or event.scan_code == 201:
                logging.debug("Sending PLAY_PAUSE")
                self.q.put("PLAY_PAUSE")
            elif event.scan_code == 163:
                logging.debug("Sending FORWARD")
                self.q.put("FORWARD")
            elif event.scan_code == 165:
                logging.debug("Sending REWIND")
                self.q.put("REWIND")
