import logging
import os
import threading
import time
from .src import poweroff
from .src import btmediakeys


def check_root():
    if os.environ['USER'] != "root":
        logging.error("This program should be runned as root/sudo!")
        exit(1)


def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(name)s %(message)s')
    check_root()
    logging.debug("PodPlayer root services is on, waiting for queues")
    while not os.path.exists('/dev/mqueue/btkeys') or not os.path.exists('/dev/mqueue/poweroff'):
        time.sleep(1)
    logging.debug('Queues detected, service is active')

    poweroff_t = threading.Thread(target=poweroff.wait_for_poweroff)
    poweroff_t.start()
    btmediakeys.BtKeys()

    poweroff_t.join()

    while True:
        time.sleep(100)


if __name__ == "__main__":
    main()
